# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
class IcaewPipeline(object):
	def __init__(self):
		client = pymongo.MongoClient('35.177.72.155', 27017)
		self.db = client['chekin']
		self.db.authenticate('sid','Error203!',mechanism='SCRAM-SHA-1')
		self.collection = self.db['test4']
		self.doc_list=[]
	def process_item(self, item, spider):
		self.doc_list.append(dict(item))
		if (len(self.doc_list) == 20):
			self.collection.insert(self.doc_list)
			self.doc_list=[]
		return item
	def spider_closed(self, spider):
		self.spiders.remove(spider.name)
		self.collection.insert(self.doc_list)
		
