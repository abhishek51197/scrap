# -*- coding: utf-8 -*-
import pymongo
import scrapy
from scrapy.selector import Selector
from bs4 import BeautifulSoup
from  ..items import IcaewItem
class FindSpider(scrapy.Spider):
	name = "find"
	def __init__(self ,initial_urls=[] ,**kwargs):
		client = pymongo.MongoClient('35.177.72.155', 27017)
		self.db = client['chekin']
		self.db.authenticate('sid','Error203!',mechanism='SCRAM-SHA-1')
		self.collection = self.db['icaew']
		self.doc_list=[]
		self.allowed_domains = ["find.icaew.com"]
		self.start_urls=[]
		super().__init__(**kwargs)
		for url in initial_urls:
			for x in range(1,30):
				self.start_urls.append(url+"/page/"+str(x))
		
		
	def parse(self, response):
		if (response.status)==200:
			doc = IcaewItem()
			soup = BeautifulSoup(response.text, 'html.parser')
			listing = soup.findAll("a", class_="btn btn-primary display-tooltip-left lt")
			doc['postcode']=response.url.split("/")[-5]
			doc['link']=[]
			for x in listing:
				urls=(x['href']).split("/")
				url="https://find.icaew.com/listings/view/listing_id/"+urls[-2]+"/"+urls[-1]
				doc['link'].append(url)
			if len(doc['link']) > 0:
				self.doc_list.append(dict(doc))
			if (len(self.doc_list) == 20):
				self.collection.insert(self.doc_list)
				self.doc_list=[]
			yield (doc)
	    	
