from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import csv
process = CrawlerProcess(get_project_settings())
initial_urls =[]
count=0

with open('england.csv','r') as data:
	sheet=csv.DictReader(data,delimiter=',')
	count=0
	limit=25000
	Bc=0
	for row in sheet:
		Bc+=1
		if (Bc==4):
			Bc=0
			url=("".join(row['Postcode'].split(" ")))
			initial_urls.append("https://find.icaew.com/search/postcode/"+url+"/within-distance/1")
			count+=1
			if count==limit:
				print("_______________________________________________________________________________")
				print('Deployed')
				process.crawl('find',initial_urls=initial_urls)
				initial_urls=[]
				count=0
	process.crawl('find',initial_urls=initial_urls)
process.start() 
